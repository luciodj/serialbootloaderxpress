/*
 * Serial BootLoader for PIC16F18855 Xpress
 *
 * File:   main.c
 * Author: Lucio Di Jasio
 *
 * Compiler: XC8, v.1.35
 *
 */

/*
Copyright (c) 2013 - 2015 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*/

#include "mcc_generated_files/mcc.h"

#define APP_START       0x200     
#define ROW_SIZE        WRITE_FLASH_BLOCKSIZE

#define cmdREBOOT       'R'
#define cmdWRITE        'W'
#define cmdACK          'A'
#define cmdNACK         'N'

inline void runApp( void)
{ // ljmp to application 
#asm
                PAGESEL     APP_START
                goto        APP_START
#endasm
}

void interrupt isr( void)
{
#asm
                PAGESEL     APP_START
                goto        (APP_START+4)    
#endasm
}

/**
 * Send a word (lsb first)
 * @param w
 */
void putw( uint16_t w)
{
    putch( w);          // lsb
    putch( w>>8);       // msb
}

/**
 *  Receive a word (lsb First)
 *  @return unsigned 16-bit value
 */
uint16_t getw( void)
{
    union  {
        uint8_t     byte[2];
        uint16_t    word;
    } r;

    r.byte[0] = getch();
    r.byte[1] = getch();
    return  r.word;
} // getw 

/**
 * Receive a block of data (words)
 */
void get_data(uint16_t* pdata, uint8_t count)
{
    while ( count-- > 0)  {
        *pdata++ = getw();
    }
} // get_data

     
void main(void)
{
    SYSTEM_Initialize();

    if ( SW1_GetValue())        // check if bootloader entry requested
        runApp();
    
    LED0_SetHigh();             // show entry into bootloader waiting for connection
    while ( !SW1_GetValue());   // ensure button released
    
    while (1)
    {
        char c = getch();
        switch( c) {

            case cmdREBOOT:         // run application
                LED0_SetLow();  
                runApp();
                break;

            case cmdWRITE:          // erase/write a row of words
              {
                uint16_t buffer[ROW_SIZE];
                uint16_t addr = getw();       // get address (word)
                
                get_data(buffer, ROW_SIZE);
                if (addr >= APP_START) {        // check no overwrite!
                    FLASH_WriteBlock(addr, buffer);
//                    FLASH_WriteWord(0x400, buffer, 0x35A);
                    putch( cmdACK);             // send back acknowledge
                }
                else 
                    putch( cmdNACK);            // send back error
              }
            break;

            default:                // ignore other chars (white space)
                break;
        } // switch
    } // main loop
}

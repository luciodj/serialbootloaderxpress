PIC16 Serial Low Side Bootloader
================================

This is a Bootloader developed for the PIC16F18855 (XPRESS board) and  uses the
serial port (9600, 8, N, 1) to communicate with a PC.

The bootloader is located between location 0 and 511 and  re-directs the
interrupt vector. Use the —offset option on the XC8 compiler to generate
compatible applications.

Enter the bootloader by keeping the SW1 button pressed when resetting the
device.

 

A simple Python command line application Boot.py is provided to assist
programming the device. Usage: Python Boot.py file.hex

 

 
